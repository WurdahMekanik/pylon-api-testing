# README! #


### What is this all about? ###
These are tools for simple exploration of Basler's Pylon5 SDK & API.


### Requirements ###
* Clearly, you must have the Pylon5 SDK installed...
* I'm using some stuff from C++11, so it would be a good idea to have a compiler that is C++11 aware.
* I might add some Qt stuff in here eventually.


### How to build ###
Using GCC:
```
#!bash

g++ -I[location of Pylon5 SDK]/include -Wl,--enable-new-dtags -Wl,-rpath,[location of Pylon5 SDK]/lib64 -L/opt/pylon5/lib64 -Wl,-E -lpylonbase -lpylonutility -lGenApi_gcc_v3_0_Basler_pylon_v5_0 -lGCBase_gcc_v3_0_Basler_pylon_v5_0 main.cpp pylongrab.cpp -o pylotest

```


### How to run ###
```
#!bash

PYLON_CAMEMU={number of cameras to emulate} ./pylotest
```