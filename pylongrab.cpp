#include <unistd.h>
#include "pylongrab.h"

using namespace GenApi;
using namespace Pylon;
using namespace std;

PylonGrab::PylonGrab()
{
    // done in header...
    //Pylon::PylonAutoInitTerm autoInitTerm;
}

//PylonGrab::~PylonGrab()
//{
//    Pylon::PylonTerminate();
//}

void PylonGrab::initTransportLayer(enumTL transportLayer)
{
    CTlFactory& TlFactory = CTlFactory::GetInstance();
    switch (transportLayer)
    {
    case eTL_USB:
        mTl = TlFactory.CreateTl( BaslerUsbDeviceClass );
        break;
    case eTL_GigE:
        mTl = TlFactory.CreateTl( BaslerGigEDeviceClass );
        break;
    case eTL_Emulation:
        mTl = TlFactory.CreateTl( BaslerCamEmuDeviceClass );
    default:
        cout << "Transport layer unsupported or not implemented." << endl;
        break;
    }
}

TlInfoList_t PylonGrab::queryTransportLayers()
{
    CTlFactory& tlFactory = CTlFactory::GetInstance();
    TlInfoList_t tlInfoList;

    // Query transport layers and print info
    int numTls = tlFactory.EnumerateTls(tlInfoList);
    if (numTls == 0)
    {
        cout << "No transport layers found!" << endl;
    }
    else
    {
        cout << endl << "Transport Layers:" << endl;
        for (const auto& tl: tlInfoList)   cout << '\t' << tl.GetFullName() << endl;
    }

    return tlInfoList;
}

DeviceInfoList_t PylonGrab::queryDevices()
{
    DeviceInfoList_t devList;

    // Query devices and print info
    int numDevices = mTl->EnumerateDevices(devList);
    if (numDevices == 0)
    {
        cout << "No devices found!" << endl;
    }
    else
    {
        cout << endl << "Devices:" << endl;
        for (const CDeviceInfo& di: devList)
        {
            cout << '\t' << di.GetFullName() << " (dev-class: " << di.GetDeviceClass() << ", s/n: " << di.GetSerialNumber() << ')' << endl;
            StringList_t props;
            di.GetPropertyNames(props);
            cout << '\t' << "Properties:" << endl;
            for (const auto& p: props)  cout << "\t\t" << p << endl;
            cout << endl;
        }
    }
    return devList;
}

StringList_t PylonGrab::querySerialNumbers(DeviceInfoList_t deviceList)
{
    StringList_t serialList;

    for (const CDeviceInfo& di: deviceList)
    {
        serialList.push_back(di.GetSerialNumber());
    }

    return serialList;
}

CInstantCamera* PylonGrab::createUsbCameraFromSerial(String_t serial)
{
    CInstantCamera * camera;
    try {
        CDeviceInfo di;
        di.SetDeviceClass(BaslerUsbDeviceClass);

        // Create camera from device info
        di.SetSerialNumber(serial);
        IPylonDevice* device = mTl->CreateDevice(di);
        camera = new CInstantCamera(device);
        // Open the camera and get a reference to its node map
        camera->Open();
        INodeMap &control = camera->GetNodeMap();
        CIntegerPtr width(control.GetNode("Width"));
        CIntegerPtr height(control.GetNode("Height"));
        // Print some info about the camera.
        cout << endl;
        cout << "Using device (model name):" << endl << '\t' << camera->GetDeviceInfo().GetModelName() << endl;
        cout << "Using device (full name):" << endl << '\t' << camera->GetDeviceInfo().GetFullName() << endl;
        cout << "Serial number:" << endl << '\t' << camera->GetDeviceInfo().GetSerialNumber() << endl;
        cout << "Width:\n\t" << width->GetValue() << endl << "Height:\n\t" << height->GetValue() << endl;
    }
    catch (const GenericException& e)
    {
        cerr << e.what() << endl;
    }

    return camera;
}

NodeList_t PylonGrab::queryDeviceNodes(CInstantCamera * camera)
{
    // Get the node map and populate list of nodes.
    INodeMap &control = camera->GetNodeMap();
    NodeList_t nodes;
    control.GetNodes(nodes);

    // Print SFNC version & list of nodes with their accessability values.
    cout << "-- SFNC version: " << camera->GetSfncVersion().getVersionString() << endl << endl;
    cout << "Nodes:" << endl;
    for (const auto& nd: nodes)
    {
        cout << '\t' << nd->GetName() << endl;
        cout << "\t\t Avail: " << IsAvailable(nd) << " Writable: " << IsWritable(nd) << " Readble: " << IsReadable(nd) << endl;
    }

    return nodes;
}

void PylonGrab::setCameraExposure(CInstantCamera * camera, double value)
{
    INodeMap &control = camera->GetNodeMap();
    CFloatPtr exposure( control.GetNode("ExposureTime") );
    cout << "Exposure value: " << exposure->GetValue() << ", Min Exposure: " << exposure->GetMin() << ", Max exposure: " << exposure->GetMax() << endl;

    // Write new exposure value.
    if ( IsWritable(exposure) )
    {
        exposure->SetValue(value);
    }

    cout << "Exposure value: " << exposure->GetValue() << ", Min Exposure: " << exposure->GetMin() << ", Max exposure: " << exposure->GetMax() << endl;
}

void PylonGrab::setCameraGain(CInstantCamera * camera, double value)
{
    INodeMap &control = camera->GetNodeMap();
    CFloatPtr gain( control.GetNode("Gain") );
    cout << "Gain value: " << gain->GetValue() << ", Min Gain: " << gain->GetMin() << ", Max gain: " << gain->GetMax() << endl;

    // Write new exposure value.
    if ( IsWritable(gain) )
    {
        gain->SetValue(value);
    }

    cout << "Gain value: " << gain->GetValue() << ", Min Gain: " << gain->GetMin() << ", Max gain: " << gain->GetMax() << endl;
}

