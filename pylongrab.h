#ifndef PYLONGRAB_H
#define PYLONGRAB_H

#include <pylon/PylonIncludes.h>
#include <pylon/usb/BaslerUsbCamera.h>
#include <pylon/gige/BaslerGigECamera.h>
#include <iostream>

using namespace Pylon;

class PylonGrab
{
private:
    PylonAutoInitTerm autoInitTerm;
    ITransportLayer* mTl;
public:
    enum enumTL {
        // transport layers can be found in pylon/DeviceClass.h
        eTL_USB,
        eTL_GigE,
        eTL_Emulation
    };
    PylonGrab();
    void initTransportLayer(enumTL transportLayer);
    CInstantCamera * createUsbCameraFromSerial(String_t serial);
    TlInfoList_t queryTransportLayers();
    DeviceInfoList_t queryDevices();
    StringList_t querySerialNumbers(DeviceInfoList_t deviceList);
    GenApi::NodeList_t queryDeviceNodes(CInstantCamera * camera);
    void setCameraExposure(CInstantCamera * camera, double value);
    void setCameraGain(CInstantCamera * camera, double value);
};

#endif // PYLONGRAB_H
