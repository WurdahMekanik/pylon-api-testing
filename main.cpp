#include <iostream>
#include <string>
#include "pylongrab.h"

using namespace std;
int main(int argc, char *argv[])
{
    if (argc != 4) {
        cout << "Expected 3 arguments, got " << argc+1 << ".\n" << endl;
        return -1;
    }

    // Instantiate PylonGrab object, thus initializing Pylon system.
    PylonGrab *thing = new PylonGrab();
    
    // Query for & create transport layer.
    thing->queryTransportLayers();
    thing->initTransportLayer(PylonGrab::eTL_USB);

    // Query devices & create USB camera from serial number.
    thing->queryDevices();
    CInstantCamera * cam1 = thing->createUsbCameraFromSerial(argv[1]);
    CInstantCamera * cam2 = thing->createUsbCameraFromSerial(argv[2]);
    
    // Set exposure to 10000
    thing->setCameraExposure(cam1, stoi(argv[3]));
    
    return 0;
}
